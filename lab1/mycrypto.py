"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: Schuller Gergely


Replace this placeholder text with a description of this module.
"""
import itertools
import math
import sys


#################
# CAESAR CIPHER #
#################

def error_handling_caesar(data):
    data = ''.join(filter(str.isalpha, data))
    if data:
        if not data.isupper():
            print("All alphabetic characters must be in uppercase")
            sys.exit(1)


def error_handling_only_uppercase_alphabetic(data):
    data = ''.join(filter(str.isalpha, data))
    if data:
        if not data.isupper():
            print("All alphabetic characters must be in uppercase")
            sys.exit(1)
    else:
        print("Input must be alphabetic")
        sys.exit(1)


def encrypt_caesar(plaintext):
    """Encrypt a plaintext using a Caesar cipher.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    # raise NotImplementedError('encrypt_caesar is not yet implemented!')

    error_handling_caesar(plaintext)

    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    encrypted = ''
    for letter in plaintext:
        if letter in letters:
            num = letters.find(letter)
            num = (num + 3) % 26
            encrypted = encrypted + letters[num]

        else:
            encrypted = encrypted + letter

    return encrypted


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    # raise NotImplementedError('decrypt_caesar is not yet implemented!')

    error_handling_caesar(ciphertext)

    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    decrypted = ''
    for letter in ciphertext:
        if letter in letters:
            num = letters.find(letter)
            num = num - 3
            if num < 0:
                num = num + 26

            decrypted = decrypted + letters[num]
        else:
            decrypted = decrypted + letter

    return decrypted


###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    # raise NotImplementedError('encrypt_vigenere is not yet implemented!')

    if keyword:
        error_handling_only_uppercase_alphabetic(keyword)
    else:
        print("Keyword must have at least one letter in it.")
        sys.exit(1)

    error_handling_only_uppercase_alphabetic(plaintext)

    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if len(keyword) < len(plaintext):
        newkey = keyword
        keylen = len(keyword)
        textlen = len(plaintext)
        while (len(newkey) + keylen) < textlen:
            newkey = newkey + keyword

        end = textlen - len(newkey)

        if end > 0:
            newkey = newkey + keyword[0:end]

        num = 0
        encryptvigenere = ''
        for actletter in plaintext:
            pos = (letters.find(actletter) + letters.find(newkey[num])) % 26
            encryptvigenere = encryptvigenere + letters[pos]
            num = num + 1

        return encryptvigenere


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    # raise NotImplementedError('decrypt_vigenere is not yet implemented!')

    if keyword:
        error_handling_only_uppercase_alphabetic(keyword)
    else:
        print("Keyword must have at least one letter in it.")
        sys.exit(1)

    error_handling_only_uppercase_alphabetic(ciphertext)

    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if len(keyword) < len(ciphertext):
        newkey = keyword
        keylen = len(keyword)
        textlen = len(ciphertext)
        while (len(newkey) + keylen) < textlen:
            newkey = newkey + keyword

        end = textlen - len(newkey)

        if end > 0:
            newkey = newkey + keyword[0:end]

        num = 0
        decryptvigenere = ''
        for actletter in ciphertext:
            pos = letters.find(actletter) - letters.find(newkey[num])
            if pos < 0:
                pos = pos + 26
            decryptvigenere = decryptvigenere + letters[pos]
            num = num + 1

        return decryptvigenere


########################################
# Scytale Cipher #
########################################


def encrypt_scytale(plaintext, circumference):
    encrypt_cipher_scytale = ''

    for col in range(circumference):

        pos = col

        while pos < len(plaintext):
            encrypt_cipher_scytale = encrypt_cipher_scytale + plaintext[pos]
            pos = pos + circumference

    return encrypt_cipher_scytale


def decrypt_scytale(ciphertext, circumference):
    rows = math.ceil(len(ciphertext) / circumference)

    if len(ciphertext) % circumference > 0:
        mod = (len(ciphertext) % circumference)
        pos = (mod + 1) * rows - 1
        for i in range(0, (circumference - mod)):
            ciphertext = ciphertext[0: pos] + ' ' + ciphertext[pos:len(ciphertext)]
            pos = pos + rows

    matrix = [ciphertext[x:x + rows] for x in range(0, len(ciphertext), rows)]

    transposed = zip(*matrix)

    return ''.join(itertools.chain(*transposed)).strip()


########################################
# Railfence Cipher #
########################################

def encrypt_railfence(plaintext, num_rails):

    if num_rails == 1:
        return plaintext

    rail = [['nope' for i in range(len(plaintext))]
            for j in range(num_rails)]

    dir_down = False
    row, col = 0, 0

    for i in range(len(plaintext)):

        if (row == 0) or (row == num_rails - 1):
            dir_down = not dir_down

        rail[row][col] = plaintext[i]
        col += 1

        if dir_down:
            row += 1
        else:
            row -= 1

    result = []
    for i in range(num_rails):
        for j in range(len(plaintext)):
            if rail[i][j] != 'nope':
                result.append(rail[i][j])

    return "".join(result)


def decrypt_railfence(ciphertext, num_rails):

    if num_rails == 1:
        return ciphertext

    rail = [[' ' for i in range(len(ciphertext))]
            for j in range(num_rails)]

    dir_down = False
    row, col = 0, 0

    for i in range(len(ciphertext)):
        if (row == 0) or (row == num_rails - 1):
            dir_down = not dir_down

        rail[row][col] = '#'
        col += 1

        if dir_down:
            row += 1
        else:
            row -= 1

    index = 0
    for i in range(num_rails):
        for j in range(len(ciphertext)):
            if (rail[i][j] == '#') and (index < len(ciphertext)):
                rail[i][j] = ciphertext[index]
                index += 1

    result = []
    row, col = 0, 0
    dir_down = False

    for i in range(len(ciphertext)):

        if (row == 0) or (row == num_rails - 1):
            dir_down = not dir_down

        result.append(rail[row][col])
        col += 1

        if dir_down:
            row += 1
        else:
            row -= 1

    return "".join(result)
