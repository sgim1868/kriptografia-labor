from lab2 import merkle_hellman


def test_encryt_decrypt():
    pivate_key = merkle_hellman.generate_private_key()
    pubkey = merkle_hellman.create_public_key(pivate_key)

    message = "Wikipromotesmeaningfultopicassociationsbetweendifferentpagesbymakingpagelinkcreationintuitivelyeasyandshowingwhe"\
              "theranintendedtargetpageexistsornot."
    bit_Message = b"Wikipromotesmeaningfultopicassociationsbetweendifferentpagesbymakingpagelinkcreationintuitivelyeasyandshowingwhe"\
              b"theranintendedtargetpageexistsornot."

    encrypted = merkle_hellman.encrypt_mh(bit_Message, pubkey)

    decryted = merkle_hellman.decrypt_mh(encrypted, pivate_key)

    if message == decryted:
        return True

    return False


if __name__ == "__main__":
    if test_encryt_decrypt():
        print("OK")
    else:
        print("Failed")

