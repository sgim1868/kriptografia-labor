from lab2 import Solitaire


def test_encryt_decrypt():
    message = "Wikipromotesmeaningfultopicassociationsbetweendifferentpagesbymakingpagelinkcreationintuitivelyeasyandshowingwhe" \
              "theranintendedtargetpageexistsornot"

    seed = 123456
    encrypted = Solitaire.encrytp_sol(message, seed)

    decryted = Solitaire.decrypt_sol(encrypted, seed)

    if message.upper() == decryted:
        return True

    return False


if __name__ == "__main__":
    if test_encryt_decrypt():
        print("OK")
    else:
        print("Failed")
