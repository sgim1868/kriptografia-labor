import socket, pickle

from lab2 import merkle_hellman

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65432  # The port used by the server


def testsameorillegalid():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        pivate_key = merkle_hellman.generate_private_key()
        pubkey = merkle_hellman.create_public_key(pivate_key)
        info = ['1234', pubkey]
        data_string = pickle.dumps(["register", info])
        s.send(data_string)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as z:
        z.connect((HOST, PORT))
        z.send(data_string)
        received1 = z.recv(1024)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as x:
        x.connect((HOST, PORT))
        info = ['65432', pubkey]
        data_string = pickle.dumps(["register", info])
        x.send(data_string)
        received2 = x.recv(1024)

    if b'This port is in use!' == received1 and b'This port is in use!' == received2:
        return True
    else:
        return False


def testrefreshillegalid():
    private_key = merkle_hellman.generate_private_key()
    pubkey = merkle_hellman.create_public_key(private_key)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as a:
        a.connect((HOST, PORT))
        info = ['65432', pubkey]
        data_string = pickle.dumps(["refresh", info])
        a.send(data_string)
        received = a.recv(1024)

    if b"Can't refresh key for this port" == received:
        return True
    else:
        return False


def testunregisteredkeyget():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data_string = pickle.dumps(["getkey", '6245'])
        s.send(data_string)
        received = s.recv(1024)
        data_arr = pickle.loads(received)

    if "This client doesn't registered!" == data_arr[0]:
        return True
    else:
        return False

def testunknownrequest():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        data_string = pickle.dumps(["something"])
        s.send(data_string)
        received1 = s.recv(1024)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as z:
        z.connect((HOST, PORT))
        data_string = pickle.dumps(["somethingelse"])
        z.send(data_string)
        received2 = z.recv(1024)

    if b'Unknown request!' == received1 and b'Unknown request!' == received2:
        return True
    else:
        return False

if __name__ == "__main__":
    if testsameorillegalid() and testrefreshillegalid() and testunregisteredkeyget() and testunknownrequest():
        print('OK')
    else:
        print('Failed')
