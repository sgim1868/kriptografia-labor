import pickle
import socket

HOST = '127.0.0.1'
PORT = 65432


class Server:

    def __init__(self):
        self.clients = {}

    def registrate(self, register_information):

        client_id = register_information[0]
        key = register_information[1]

        if client_id in self.clients or '65432' == client_id:
            return False
        else:
            self.clients[client_id] = key
            return True

    def getkey(self, client_info):

        if client_info in self.clients:
            return True, self.clients[client_info]
        else:
            return False, ()

    def refreshkey(self, client_info):
        client_id = client_info[0]
        key = client_info[1]

        if '65432' == client_id:
            return False, ''
        else:
            if client_id in self.clients:
                ret_message = b"Successful key refresh"
            else:
                ret_message = b"You were unregistered!Successful registration with this key"

            self.clients[client_id] = key
            return True, ret_message

    def runserver(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            print('Server started ')
            while True:
                s.listen()
                conn, address = s.accept()
                with conn:
                    data = conn.recv(4096)
                    data_arr = pickle.loads(data)
                    request = data_arr[0]

                    print("Received from", address, " ", repr(data_arr))

                    if request != "refresh" and request != "getkey" and request != "register":
                        print("Server sent: Unknown request! to: ", address)
                        conn.sendall(b"Unknown request!")

                    if "register" == request:
                        register_ok = self.registrate(data_arr[1])
                        if register_ok:
                            print("Server sent: Successful registration! to: ", address)
                            conn.sendall(b"Successful registration!")
                        else:
                            print("Server sent: This port is in use! to: ", address)
                            conn.sendall(b"This port is in use!")

                    if "getkey" == request:
                        get_ok, key = self.getkey(data_arr[1])
                        if get_ok:
                            data_string = pickle.dumps(key)
                            print("Server sent:", data_string, "to: ", address)
                            conn.send(data_string)
                        else:
                            print("Server sent: This client doesn't registered! to: ", address)
                            data_string = pickle.dumps(["This client doesn't registered!"])
                            conn.sendall(data_string)

                    if "refresh" == request:
                        refresh_ok, message = self.refreshkey(data_arr[1])
                        if refresh_ok:
                            print("Server sent:", message, " to: ", address)
                            conn.sendall(message)
                        else:
                            print("Server sent: Can't refresh key for this port to: ", address)
                            conn.sendall(b"Can't refresh key for this port")


if __name__ == "__main__":
    server = Server()
    server.runserver()
