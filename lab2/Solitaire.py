# generated the suffled deck 53 and 54 are the jokers
import random
import sys


def generate_start_deck(seed):
    deck = list(range(1, 55))
    random.Random(seed).shuffle(deck)

    return deck


def find_output_value(deck):
    first_joker_place = deck.index(53)

    if first_joker_place != 53:
        deck[first_joker_place] = deck[first_joker_place + 1]
        deck[first_joker_place + 1] = 53
    else:
        deck = [deck[0]] + [deck[53]] + deck[1:-1]

    second_joker_place = deck.index(54)

    if second_joker_place != 53 and second_joker_place != 52:
        deck[second_joker_place] = deck[second_joker_place + 2]
        deck[second_joker_place + 2] = 54
    else:
        if second_joker_place == 52:
            deck = [deck[0]] + [deck[52]] + deck[1:-2] + [deck[53]]
        else:
            deck = deck[0:2] + [deck[53]] + deck[1:-1]

    # triple cut

    second_joker_place = deck.index(54)

    first_joker_place = deck.index(53)

    if (first_joker_place < second_joker_place):

        first_part = deck[0:first_joker_place]

        second_part = deck[first_joker_place:second_joker_place + 1]

        third_part = deck[second_joker_place + 1:54]

    else:

        first_part = deck[0:second_joker_place]
        second_part = deck[second_joker_place:first_joker_place + 1]

        third_part = deck[first_joker_place + 1:54]

    deck = third_part + second_part + first_part

    # count cut

    buttom_value = deck[53]

    deck = deck[buttom_value:-1] + deck[0:buttom_value] + [deck[53]]

    # get the output value

    if deck[0] == 53 or deck[0] == 54:
        return find_output_value(deck)

    output_value = deck[deck[0]]

    return output_value, deck


# I'm using uppercase letters only
def encrytp_sol(message, seed):
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    message = message.upper()

    if not isinstance(seed, int):
        print("wrong private key")
        sys.exit(1)

    deck = generate_start_deck(seed)

    encrypted = ''
    for letter in message:
        outputvalue, deck = find_output_value(deck)

        encrypted += letters[(letters.find(letter) + outputvalue) % 26]

    return encrypted


# I'm using uppercase letters only
def decrypt_sol(message, seed):
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    message = message.upper()

    if not isinstance(seed, int):
        print("wrong private key")
        sys.exit(1)

    deck = generate_start_deck(seed)

    decrypted = ''
    for letter in message:
        outputvalue, deck = find_output_value(deck)

        num = letters.find(letter)
        num = num - outputvalue
        if num < 0:
            num = num + 26

        decrypted = decrypted + letters[num]

    return decrypted
