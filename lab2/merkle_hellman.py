import random

import sys

from lab2 import utils


# Build a superincreasing sequence `w` of length n
def generate_superincreasing_sequence(n=8):
    w = [random.randint(1, 9)]
    w_length = 1
    total = w[0]
    while w_length < n:
        w_next = random.randint(total + 1, 2 * total)
        total += w_next
        w.append(w_next)
        w_length += 1

    if not utils.is_superincreasing(w):
        w = generate_superincreasing_sequence(n)

    return w


# generates a coprime with q
def generete_coprime(q):
    rand_range = q - 1
    r = random.randint(2, rand_range)
    while not utils.coprime(r, q):
        r = random.randint(2, rand_range)

    return r


def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints
    """

    if n <= 0:
        print("n must be nonzero positve number")
        sys.exit(1)

    w = generate_superincreasing_sequence(n)

    w_sum = sum(w)
    q = random.randint(w_sum + 1, 2 * w_sum)

    r = generete_coprime(q)

    return w, q, r


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """

    if not isinstance(private_key[0], list):
        print("wrong private key")
        sys.exit(1)

    w = private_key[0]
    q = private_key[1]
    r = private_key[2]
    beta = [r * w_i % q for w_i in w]

    return tuple(beta)


# It ecryptsusing n = 8
def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """

    if not isinstance(message, bytes):
        print("wrong message type ")
        sys.exit(1)

    if not isinstance(public_key, tuple):
        print("wrong public_key type ")
        sys.exit(1)

    if len(public_key) != 8:
        print("I'm encryting with 8 bit sized message wrong public_key type ")
        sys.exit(1)

    encrypted = []
    for byte in message:
        bits = utils.byte_to_bits(byte)
        c = sum(bits[i] * public_key[i] for i in range(8))
        encrypted.append(c)

    return encrypted


# Im using n = 8
def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """

    if not isinstance(private_key[0], list):
        print("wrong private key")
        sys.exit(1)

    w = private_key[0]
    q = private_key[1]
    r = private_key[2]

    s = utils.modinv(r, q)

    decrypted = bytearray(b'')

    for byte in message:
        c_comma = (byte * s) % q
        bits = []
        for i in range(7, -1, -1):
            if w[i] > c_comma:
                bits.append(0)
            else:
                bits.append(1)
                c_comma -= w[i]

        # Im reversing the bits
        reverse_bits = bits[::-1]
        decrypted.append(utils.bits_to_byte(tuple(reverse_bits)))

    return decrypted.decode()
